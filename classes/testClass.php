<?php

class TestClass {
	
	use TestTrait;
	use AnotherTrait;

	public function __construct() {

	}

	public function isEddWorking() {

		return "Edd is " . $this->whatIsEddDoing() . 

			"\n" . $this->why();
	}
}